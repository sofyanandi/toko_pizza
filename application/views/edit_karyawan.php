<?php
	foreach($detail_karyawan as $data) 
	{
    	$nik			= $data->nik;
		$nama_lengkap	= $data->nama;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir		= $data->tanggal_lahir;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
    }
	$thn_pisah = substr($tgl_lahir, 0,4);
	$bln_pisah = substr($tgl_lahir, 5,2);
	$tgl_pisah = substr($tgl_lahir, 8,2);
?>
<body bgcolor="#999999">
<center><font color="#FFFFFF" size="+3">Edit Karyawan</font></center><br>
<form action="<?=base_url()?>karyawan/editkaryawan/<?=$nik; ?>" method="POST";>
<table width="50%" border="0" cellpadding="5" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td><input value ="<?=$nik;?>" type="text" name="nik" id="nik" maxlength="10" readonly></td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td><input value="<?=$nama_lengkap;?>" type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50"></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" ><?=$alamat;?></textarea></td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input value="<?=$telp;?>"type="text" name="telp" id="telp" maxlength="50"></td>
  </tr>
  
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input value="<?=$tempat_lahir;?>" type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tanggal">
    	<?php
        for($tgl=1;$tgl<=31; $tgl++){
			$select_tgl = ($tgl == $tgl_pisah) ? 'selected' : '';
			?>
        <option value="<?=$tgl;?>" <?=$select_tgl;?>><?=$tgl;?></option>
        <?php
        }
		?>
    </select>
      
      <select name="bln" id="bulan">
      <?php
      	$bulan = array ('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$b=0;
		while(each($bulan)){
			if($b+1== $bln_pisah){
				$n = 'SELECTED';
			}else{
				$n = '';
			}		
	  ?>
      <option <?=$n;?> value="<?=$b+1;?>" ><?=$bulan[$b];?></option>
      <?php
      	$b++;
		}
	  ?>
      </select>
      <select name="thn" id="tahun">
      <?php
		for($thn = 1990; $thn <= date('Y'); $thn++){$select_thn = ($thn == $thn_pisah) ? 'selected' : '';				
		?>
		<option value="<?=$thn;?>" <?=$select_thn?>><?=$thn;?></option>
					
		<?php
		}
		?>
      </select>
      </td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="submit" id="submit" value="Simpan">
      <input type="reset" name="reset" id="reset" value="Reset"><br/><br/>
      <a href="<?=base_url();?>karyawan/listkaryawan""><input type="button" name="button" id="button" value="Kembali Ke Menu Sebelumnya"></a></td>
  </tr>
</table>
</form>
</body>