<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan_model extends CI_Model 
{
	
	public function __construnct()
	{
		parent::__construnct();
		
		$this->load->model("menu_model");	
	}
	//panggil nama table
	private $_table = "transaksi_pemesanan";
	
	public function tampilDataPemesanan()
	
	{
		return $this->db->get($this->_table)->result();	
	}
	
	public function tampilDataPemesanan2()
	{
	$query =$this->db->query("SELECT * FROM transaksi_pemesanan as tp 
	inner join karyawan as kr on tp.nik=kr.nik 
	inner join menu as mn ON tp.kode_menu= mn.kode_menu");
		return $query->result();
	}
	
	public function savePemesanan($id)
	{
		$pegawai					= $this->input->post('nik');
		$nama_pelanggan				= $this->input->post('nama');
		$kode_menu					= $this->input->post('kode_menu');
		$qty						= $this->input->post('qty');
		$hargamenu					= $this->menu_model->cariHargaMenu($kode_menu);
		
		$data['id_pemesanan']						= $id;
		$data['nik']								= $pegawai;
		$data['tgl_pemesanan']						= date('Y-m-d');
		$data['nama_pelanggan']						= $nama_pelanggan;
		$data['kode_menu']							= $kode_menu;
		$data['qty']								= $qty;
		$data['total_harga']						= $qty * $hargamenu;
		
		$this->db->insert($this->_table, $data);
	}
	
	
	public function delete($id_pemesanan)
	
	{
		$this->db->where('id_pemesanan',$id_pemesanan);
		$this->db->delete($this->_table);
	}
	
	
	
}
