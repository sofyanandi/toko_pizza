<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model("menu_model");
	}
	public function index()
	{
		$this->listMenu();
	}
	public function listMenu()
	{
		$data['data_menu'] = $this->menu_model->tampilDataMenu2();
		$this->load->view('menu', $data);
	}
	
	
	public function input_menu()
	{
			if (!empty($_REQUEST)) {
				$m_menu = $this->menu_model;
				$m_menu->save();
				redirect("menu/index", "refresh");	
			}
		
		$this->load->view('input_menu');
	}
	public function detailMenu($kode_menu)
	{
		$data['detail_menu'] = $this->menu_model->detail($kode_menu);
		$this->load->view('detail_menu', $data);	
	}
	
	public function editMenu($kode_menu)
	{	
		$data['detail_menu']			= $this->menu_model->detail($kode_menu);
		
		
		if (!empty($_REQUEST)) {
				$m_menu = $this->menu_model;
				$m_menu->update($kode_menu);
				redirect("menu/index", "refresh");	
			}
		
		$this->load->view('edit_menu', $data);	
	}
	public function delete($kode_menu)
	{
		$m_menu = $this->menu_model;
		$m_menu->delete($kode_menu);	
		redirect("menu/index", "refresh");	
	}
	
	
	
}
