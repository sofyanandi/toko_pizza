<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan extends CI_Controller {
	
	public function __construct()
	{
		parent:: __construct();
		$this->load->model("pemesanan_model");
		$this->load->model("karyawan_model");
		$this->load->model("menu_model");
		
	}
	
	public function index()
	{
		$this->listPemesanan();
	}
	public function listPemesanan()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_menu'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_pemesanan'] = $this->pemesanan_model->tampilDataPemesanan2();
		$this->load->view('pemesanan', $data);
	}
	
	
	public function input_pemesanan()
	{	
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_menu'] 	= $this->menu_model->tampilDataMenu();
		if (!empty($_REQUEST)) {
				$m_pemesanan = $this->pemesanan_model;
				$m_pemesanan->savePemesanan();
				
				redirect("pemesanan/listpemesanan");	
			}
		$this->load->view('input_pemesanan', $data);
	}
	
	public function delete($id_pemesanan)
	{
		$m_pemesanan = $this->pemesanan_model;
		$m_pemesanan->delete($id_pemesanan);	
		redirect("pemesanan/index", "refresh");	
	}
	
	
}
